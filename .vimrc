" Set syntax
if has("syntax")
  syntax on
endif

set number

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
set background=dark
set t_Co=256

set t_8b=48;2;%lu;%lu;%lum
set t_8f=38;2;%lu;%lu;%lum

set pastetoggle=<F2>
